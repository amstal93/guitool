from django.http import HttpResponse
import datetime
from django.shortcuts import render
import os
import sys, subprocess
from django.contrib import messages
from urllib.parse import urlparse, parse_qs
import re
from django.core.files.base import ContentFile
from django.core.files import File
import shutil
import time
import random
from .models import Herramientas

#####################################
#           My functions
#####################################
RESERVED_WORDS_PSQL = [
    'ALL','ANALYSE','ANALYZE','AND','ANY','ARRAY','AS','ASC','ASYMMETRIC',
    'AUTHORIZATION','BINARY','BOTH','CASE','CAST','CHECK','COLLATE','COLLATION',
    'COLUMN','CONCURRENTLY','CONSTRAINT','CREATE','CROSS','CURRENT_CATALOG','CURRENT_DATE',
    'CURRENT_ROLE','CURRENT_SCHEMA','CURRENT_TIME','CURRENT_TIMESTAMP','CURRENT_USER',
    'DEFAULT','DEFERRABLE','DESC','DISTINCT','DO','ELSE','END','EXCEPT','FALSE',
    'FETCH','FOR','FOREIGN','FREEZE','FROM','FULL','GRANT','GROUP','HAVING','ILIKE',
    'IN','INITIALLY','INNER','INTERSECT','INTO','IS','ISNULL','JOIN','LATERAL','LEADING',
    'LEFT','LIKE','LIMIT','LOCALTIME','LOCALTIMESTAMP','NATURAL','NOT','NOTNULL','NULL',
    'OFFSET','ON','ONLY','OR','ORDER','OUTER','OVERLAPS','PLACING','PRIMARY','REFERENCES',
    'RETURNING','RIGHT','SELECT','SESSION_USER','SIMILAR','SOME','SYMMETRIC','TABLE','THEN',
    'TO','TRAILING','TRUE','UNION','UNIQUE','USER','USING','VARIADIC','VERBOSE','WHEN','WHERE',
    'WINDOW','WITH'
    ]

ALL_FRASES = [
    ['"Bon dia! Pantaallaa en blanc i negreee"','- Eduard Canet'],
    ['"Esto no os lo tendría que enseñar, pero os lo enseño. No digais nada."','- Alberto Larraz'],
    ['"Y para mañana de deberes la practica 5,6,7,8,9,12,11,13,14,15,16 i si podeis, la 17."','- Montse Soler'],
    ['"Refinament i joc de proves"','- Anna Cots'],
    ['"No es un bug, es una característica no documentada."','- Anonimo'],
    ['"Para entender la recursividad, primero se ha de entender la recursividad."','- Anonimo'],
    ]


def popen_stdout(command_list):
    '''
    Recieve a command with in list format and return this stdout
    '''
    resultado = subprocess.Popen(
        command_list, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = resultado.communicate()
    resultado = out + err
    resultado = resultado.decode('ascii')
    return resultado

def popen_returncode(command_list):
    '''
    Recieve a list with path script and his arguments and return the exit code of bash script
    '''
    result = subprocess.Popen(command_list)
    text = result.communicate()[0]
    return result.returncode

def system_particularities(so_base, fields):
    '''
    Que fa? Crea lo que seran les ordrse de instalacio de cada paquet segons el so que es vol crear.
    Entrada: so_base string que es el sistema operatiu i fields diccionai que es la query post.
    Sortida: retorna la cadena que sera incorporada al dockerfile dels installs i updates necessaris.
    '''
    SISTEMES = {
        'fedora:28': {
            'first_commands': 'RUN dnf -y update vi',
            'install_command': 'RUN dnf -y install',
            'packages': {
                'basic_utils':'vim procps mlocate man-db tree',
                'net_utils':'nmap telnet iputils iproute telnet-server',
                'ssh':'openssh openssh-server openssh-clients',
                'xinetd':'xinetd'
            }
        },
        'ubuntu:16.04': {
            'first_commands': 'RUN apt-get update',
            'install_command': 'RUN apt-get -y install',
            'packages': {
                'basic_utils':'vim procps mlocate man-db tree',
                'net_utils':'nmap iputils-ping iproute telnet telnetd',
                'ssh':'openssh-server openssh-client',
                'xinetd':'xinetd'
            }
        },
        'debian:7-slim': {
            'first_commands': 'RUN apt-get update',
            'install_command': 'RUN apt-get -y install',
            'packages': {
                'basic_utils':'vim procps mlocate man-db tree',
                'net_utils':'nmap iputils-ping iproute telnet telnetd',
                'ssh':'openssh-server openssh-client',
                'xinetd':'xinetd'
            }
        },
        'opensuse:42.3': {
            'first_commands': 'RUN zypper --non-interactive up',
            'install_command': 'RUN zypper --non-interactive install',
            'packages': {
                'basic_utils':'vim procps mlocate tree',
                'net_utils':'nmap telnet iputils iproute telnet-server',
                'ssh':'openssh',
                'xinetd':'xinetd'
            }        
        },
    }

    # Si el usuario ha entrado un base propia, haz match con alguno de los anteriores. 
    # Si es otro no te la jueges y no instales ningun paquete.
    if 'fedora' in so_base:
        so_base = 'fedora:28'
    elif 'opensuse' in so_base:
        so_base = 'opensuse:42.3'
    elif 'ubuntu' in so_base:
        so_base = 'ubuntu:16.04'
    elif 'debian' in so_base:
        so_base = 'debian:7-slim'
    else:
        to_install=''
        return to_install

    # Agaf paquets que usuari vol instalar
    paquetes = fields.get('paquetes')
    installs = ''
    # Per cada paquet, agafa realment quins son
    for paquete in paquetes:
        installs += SISTEMES[so_base]['packages'][paquete]
    # Estableix quin sera la primera ordre update
    first_comm = SISTEMES[so_base]['first_commands']
    # Quin es el gestor de paquets
    install_comm = SISTEMES[so_base]['install_command']
    # Crea la sentencia global dinstalacio
    to_install = "{}\n{} {}".format(first_comm, install_comm, installs)

    return to_install


def build_bbdd_script(fields, bbdd):
    '''
    Creates de BBDD script.
    Entrada: fields (dictionary with all data), bbdd (string with the bbdd name)
    Salida: String
    '''
    final_database = bbdd
    final_tables = fields.get('tables:')
    final_script = ""
    table_script = ""
    alter_tables = ""
    fk_alters_script = ""
    pk_alters_script = ""

    # Por cada tabla, recupera sus campos y la primary key.
    for table in final_tables:
        final_campos = fields.get("{}:campos:".format(table))
        final_pk = fields.get("{}:otros:pk".format(table))
        # Si existe PK, crea la sentencia.
        if final_pk:
            final_pk = 'ALTER TABLE {} ADD PRIMARY KEY ({});\n'.format(table, final_pk[0])
        else:
            final_pk = ''
        # Conctatena las seltencias PK de las diferentes tablas
        pk_alters_script += final_pk
        # Por cada campo de una tabla
        for campo in final_campos:
            # Obten su tipo de dato
            final_tipos = fields.get("{}:{}:tipo".format(table, campo))[0]
            # Obten si es unique
            final_uk = fields.get("{}:{}:otros:uk".format(table, campo))
            if final_uk:
                final_uk = 'UNIQUE'
            else:
                final_uk = ''
            # Obten si es not null
            final_nn = fields.get("{}:{}:otros:nn".format(table, campo))
            if final_nn:
                final_nn = 'NOT NULL'
            else:
                final_nn = ''
            # Concatenalo todo
            table_script += " {} {} {} {} ,\n".format(
                campo, final_tipos, final_uk, final_nn)
            
            # Obten tambien si es una FK. Si lo es, crea la sentencia
            final_fk = fields.get("{}:{}:otros:fk".format(table, campo))
            if final_fk:
                final_fk = final_fk[0].split(':')
                final_fk = "ALTER TABLE {}\n ADD CONSTRAINT {}{}_fk FOREIGN KEY ({}) REFERENCES {}({});\n".format(
                    table, table, campo, campo, final_fk[0],final_fk[1])
            else:
                final_fk = ''
            # Concatena todas las sentencias FK
            fk_alters_script += final_fk

        # Una vez se han procesado los campos de la tabla. Completa el create table total.
        table_script = "CREATE TABLE {} (\n {} \n);\n".format(table, table_script[:-2])
        # Concatenalo al resto de create tables
        final_script = "{}\n{}".format(final_script, table_script)
        table_script = ""

    # Completa finalmente el create database, con los alters que necsitan ir al final.
    final_script = "CREATE DATABASE {};\n\c {}\n{}\n{}\n{}".format(
        final_database,final_database, final_script, pk_alters_script, fk_alters_script)

    # Pretty. Elimina los espacios duplicados.
    final_script = re.sub(r' {2,}',' ', final_script)

    return final_script


def refresh_tables_dict(tables, fields):
    '''
    Guarda los valores ya introducidos en el diccionatio tables. Para que se guarden o los vacie si se han quitado.
    Entrada: tables (dict with all current data) and fields (response data dict)
    Salida: none
    '''
    # Por cada tabla en TABLES
    for pre_table in tables.keys():
        if pre_table+':campos:' in fields:
            # Por cada campo de esta tabla
            for campo in fields[pre_table+':campos:']:
                # Actualiza la informacion interna de los campos
                if pre_table+':'+campo+':tipo' in fields:
                    tables[pre_table][campo]['tipo'] = fields[pre_table+':'+campo+':tipo']
                else:
                    tables[pre_table][campo]['tipo'] = ''
                if pre_table+':otros:pk' in fields:
                    tables[pre_table][campo]['pk'] = fields[pre_table+':otros:pk']
                else:
                    tables[pre_table][campo]['pk'] = []
                if pre_table+':'+campo+':otros:uk' in fields:
                    tables[pre_table][campo]['uk'] = fields[pre_table+':'+campo+':otros:uk']
                else:
                    tables[pre_table][campo]['uk'] = ''
                if pre_table+':'+campo+':otros:nn' in fields:
                    tables[pre_table][campo]['nn'] = fields[pre_table+':'+campo+':otros:nn']
                else:
                    tables[pre_table][campo]['nn'] = ''

def build_frases():
    '''
    Genera las citas computacionales random
    Entrada: Nada
    Salida: Lista de las frases a enviar.
    '''
    send_frases = []

    # Obtengo numero inicio frases
    num_ini = random.randint(0,len(ALL_FRASES)-1)

    # Para coger las 5 siguientes
    pos = 0
    p = 0
    for i in range(3):
        pos = num_ini + p
        if pos >= len(ALL_FRASES)-1:
            send_frases.append(ALL_FRASES[pos])
            pos = 0
            p = -1
            num_ini = 0
        else:
            send_frases.append(ALL_FRASES[pos])
        p += 1

    return send_frases

#####################################
#        VIEWS functions
#####################################

def base(request):

    send_frases = build_frases()

    return render(request, 'base.html', {'citas':send_frases})

def create_user(request):
    # Set possible messages to show
    error_messages = [
        ['Usuario creado con exito', 'good'],
        ['Error al crear un usuario sin grupos secundarios y con el grupo principal por defecto', 'bad'],
        ['Error al crear un usuario con grupos secundarios y con el grupo principal por defecto', 'bad'],
        ['Error al crear un usuario sin grupos secundarios y con un grupo principal especifico', 'bad'],
        ['Error al crear un usuario con grupos secundarios y con un grupo principal especifico', 'bad'],
        ['Error al establecer el nombre real al usuario', 'bad'],
        ['Error al establecer la direccion al usuario', 'bad'],
        ['Error al establecer la direccion al usuario', 'bad'],
        ['Error al establecer la password al usuario', 'bad'],
        ['Nombre de usuario invalido. Puede que ya exista..', 'bad'],
        ['Las passwords no coinciden', 'bad'], 
        ['Ruta de directorio incorrecta', 'bad'], 
        ['Ha habido algun error no identificado', 'bad'], 
        ['Falta algun campo obligatorio', 'bad'],
    ]

    send_frases = build_frases()

    # Get actual system groups for show in template selects
    groups = []
    groups_cat = popen_stdout(['cat', '/etc/group'])
    groups_cat = groups_cat.split('\n')
    for line in groups_cat:
        groups.append(line.split(':')[0])

    # Get actual system usernames for comprovations
    usernames = []
    usernames_cat = popen_stdout(['cat', '/etc/passwd'])
    usernames_cat = usernames_cat.split('\n')
    for line in usernames_cat:
        usernames.append(line.split(':')[0])

    # When form's send
    if request.GET.get('Go'):
        # Get all fields
        parsed_url = urlparse(request.build_absolute_uri())
        fields = parse_qs(parsed_url.query)
        if 'username' not in fields or 'password1' not in fields or 'password2' not in fields:
            return render(
                request, 'create_user.html',
                {'citas':send_frases, 'groups':groups, 'messages':error_messages[13]})
        else:
            username = fields.get('username')[0].strip()
            password1 = fields.get('password1')[0].strip()
            password2 = fields.get('password2')[0].strip()
        group_main = fields.get('group')[0]
        if 'groups' in fields:
            groups_sec = fields.get('groups')
        else:
            groups_sec = []
        if 'homedir' in fields:
            home_dir = fields.get('homedir')[0].strip()
        else:
            home_dir = 'default'
        if 'fullname' in fields:
            nombre = fields.get('fullname')[0].strip()
        else:
            nombre = 'None'
        if 'direccion' in fields:
            direccion = fields.get('direccion')[0].strip()
        else:
            direccion = 'None'
        if 'telefono' in fields:
            telefono = fields.get('telefono')[0].strip()
        else:
            telefono = 'None'
        # From's comprovations:
        # Username not empty and not exist in system:
        if username == '' or username in usernames:
            return render(
                request, 'create_user.html', 
                {'citas':send_frases, 'groups':groups, 'messages':error_messages[9]})
        # Passwords field maching. Not empty. Not null.
        if password1 == '' or password1 != password2:
            return render(
                request, 'create_user.html',
                {'citas':send_frases, 'groups':groups, 'messages':error_messages[10]})
        # If user put default in homedir field: homedir are /home/username.
        if home_dir == 'default' or home_dir == '':
            home_dir = '/home/' + username
        # Else if user write a dir, chech if is correct path. If not, render with error. If yes, homedir already set.
        elif not bool(re.match('^\/[a-zA-Z]+(?:\/[a-zA-Z]+){0,}[^\/]*$', home_dir)):
            return render(
                request, 'create_user.html',
                {'citas':send_frases, 'groups':groups, 'messages':error_messages[11]})
        # If still alive, create user. Before, put groups sec in string separated with comas for pas to script useradd.
        groups_sec_str = ''
        for group in groups_sec:
            groups_sec_str += group + ','
        if groups_sec_str == '' or groups_sec_str == ',':
            groups_sec_str = 'None'
        else:
            groups_sec_str = groups_sec_str[:-1]
        # Go and get return code
        returncode = popen_returncode(
            ["/etc/guitool/script_programs/create_user.sh",  username, password1,
            group_main, groups_sec_str, home_dir, nombre, direccion, telefono])
        if returncode == 0:
            return render(
                request, 'create_user.html',
                {'citas':send_frases, 'groups':groups, 'messages':error_messages[0]})
        elif returncode == 1:
            return render(
                request, 'create_user.html',
                {'citas':send_frases, 'groups':groups, 'messages':error_messages[1]})            
        elif returncode == 2:
            return render(
                request, 'create_user.html',
                {'citas':send_frases, 'groups':groups, 'messages':error_messages[2]})            
        elif returncode == 3:
            return render(
                request, 'create_user.html',
                {'citas':send_frases, 'groups':groups, 'messages':error_messages[3]})            
        elif returncode == 4:
            return render(
                request, 'create_user.html',
                {'citas':send_frases, 'groups':groups, 'messages':error_messages[4]})            
        elif returncode == 5:
            return render(
                request, 'create_user.html',
                {'citas':send_frases, 'groups':groups, 'messages':error_messages[5]})            
        elif returncode == 6:
            return render(
                request, 'create_user.html',
                {'citas':send_frases, 'groups':groups, 'messages':error_messages[6]})            
        elif returncode == 7:
            return render(
                request, 'create_user.html',
                {'citas':send_frases, 'groups':groups, 'messages':error_messages[7]})            
        elif returncode == 8:
            return render(
                request, 'create_user.html',
                {'citas':send_frases, 'groups':groups, 'messages':error_messages[8]})            
        else:
            return render(
                request, 'create_user.html',
                {'citas':send_frases, 'groups':groups, 'messages':error_messages[12]})
    # Render for first time
    return render(
        request, 'create_user.html',
        {'citas':send_frases, 'groups':groups, 'messages':None})


def create_backup(request):
    # Set possible messages to show
    error_messages = [
        ['Rutina de backup creada con exito', 'good'],
        ['El directorio especificado no existe', 'bad'],
        ['El directorio donde guardar los backups (/root/Backups_guitool) no existe', 'bad'],
        ['Error al ejecutar el backup con la opcion crear ahora', 'bad'],
        ['Esta rutina de backup ya existe', 'bad'], 
        ['Falta algun campo obligatorio', 'bad'],
        ['Ha habido algun error no identificado', 'bad'], 
    ]

    send_frases = build_frases()

    # When form's send
    if request.GET.get('Go'):
        # Get all fields and form's comprovations
        parsed_url = urlparse(request.build_absolute_uri())
        fields = parse_qs(parsed_url.query)
        # Required fields
        if 'directorio' not in fields or 'timing' not in fields:
            return render(
                request, 'create_backup.html',
                {'citas':send_frases, 'messages':error_messages[5]})
        else:
            directorio = fields.get('directorio')[0].strip()
            timing = fields.get('timing')[0].strip()
        if 'ahora' in fields:
            ahora = fields.get('ahora')[0]
        else:
            ahora = 'no'
        # Go and get return code
        returncode = popen_returncode(["/etc/guitool/script_programs/create_backups.sh", directorio, ahora, timing])
        if returncode == 0:
            return render(
                request, 'create_backup.html',
                {'citas':send_frases, 'messages':error_messages[0]})
        elif returncode == 1:
            return render(
                request, 'create_backup.html',
                {'citas':send_frases, 'messages':error_messages[1]})
        elif returncode == 2:
            return render(
                request, 'create_backup.html',
                {'citas':send_frases, 'messages':error_messages[2]})
        elif returncode == 3:
            return render(
                request, 'create_backup.html',
                {'citas':send_frases, 'messages':error_messages[3]})
        elif returncode == 4:
            return render(
                request, 'create_backup.html',
                {'citas':send_frases, 'messages':error_messages[4]})
        else:
            return render(
                request, 'create_backup.html',
                {'citas':send_frases, 'messages':error_messages[6]})
    # Render for first time
    return render(
        request, 'create_backup.html',
        {'citas':send_frases, 'messages':None})



def create_docker(request):
    # Set possible messages to show.
    error_messages = [
        ['Container creado con exito', 'good'],
        ['La base del container especificada no esta disponible', 'bad'],
        ['El nombre o hostname del container es invalido o ya esta en uso', 'bad'],
        ['Falta algun campo obligatorio', 'bad'],
        ['Docker no puede arrancar. ¿Lo tiene instalado en el sistema?', 'bad'],
        ['Ruta del directorio de trabajo incorrecta', 'bad'],
        ['Ha habido algun error no identificado', 'bad'], 
    ]

    send_frases = build_frases()
    
    networks = []

    # Start docker. If cant, docker maybe no exists.
    start_docker = popen_returncode(['systemctl', 'start', 'docker'])
    time.sleep(2)
    if start_docker != 0:
        return render(
            request, 'create_docker.html',
            {'citas':send_frases, 'networks': networks, 'messages':error_messages[4]})

    # Get actual docker networks in the system
    networks_cat = popen_stdout(['docker', 'network', 'ls'])
    networks_cat = networks_cat.split('\n')[1:-1]
    for line in networks_cat:
        line = re.sub(' +', ' ', line).split(' ')[1]
        if line != 'host':
            networks.append(line)

    # When form's send
    if request.POST.get('Go'):
        # Get all values of post method. Save in dict.
        fields = {}
        for key, values in request.POST.lists():
            fields[key]=values
        # Required fields
        if 'so_base' not in fields: # If not so is selected
            return render(
                request, 'create_docker.html',
                {'citas':send_frases, 'networks': networks, 'messages':error_messages[3]})
        else: # If so is selected
            # Get so
            so_base = fields.get('so_base')[0].strip()
            if so_base == 'otra': # If user set custom base
                so_base = fields.get('base_other')[0].strip()
            # IMPORTANT Once we have so base, if user wants to install some pack, call function and instal based Once so_base.
            if fields.get('paquetes'):
                # Construeix el que sera lscript que fara els installs del docker. Ja es te en compte la base.
                to_install = system_particularities(so_base,fields)
            else:
                to_install = ''

        if not fields.get('name')[0].strip() or not fields.get('hostname')[0].strip():
            return render(
                request, 'create_docker.html',
                {'citas':send_frases, 'networks': networks, 'messages':error_messages[3]})
        else:
            name = fields.get('name')[0].strip()
            hostname = fields.get('hostname')[0].strip()
        if fields.get('network')[0].strip() != '':
             network = fields.get('network')[0]
        else:
            network = 'None'
        if fields.get('workdir')[0].strip():
            workdir = fields.get('workdir')[0]
            # Check if workdir field is valid route
            if workdir and not bool(re.match('^\/[a-zA-Z]+(?:\/[a-zA-Z]+){0,}[^\/]*$', workdir)):
                return render(
                    request, 'create_docker.html',
                    {'citas':send_frases, 'networks': networks, 'messages':error_messages[5]})
        else:
            workdir = 'None'

        if fields.get('privileged'):
            privileged = 'true'
        else:
            privileged = 'false'

        # Create the folder if it doesn't exist.
        try:
            os.mkdir(os.path.join('/etc/guitool/tmp'))
        except:
            pass
        if request.FILES.get('files'):
            for myfile in request.FILES.getlist('files'):
                # Remove previous file and save the uploaded file inside that folder.
                full_filename = os.path.join('/etc/guitool/tmp', myfile.name)
                fout = File(open(full_filename, 'wb+'))
                # Iterate lines.
                for line in myfile.readlines():
                    fout.write(line)
                fout.close()
        else:
            files = 'None'

        # Go and get return code
        returncode = popen_returncode(
            ["/etc/guitool/script_programs/create_docker.sh", so_base, name,
            hostname, network, workdir, to_install, privileged])
        if returncode == 0: # ok
            return render(
                request, 'create_docker.html',
                {'citas':send_frases,'networks': networks, 'messages':error_messages[0]})
        elif returncode == 2: # docker run
            return render(
                request, 'create_docker.html',
                {'citas':send_frases,'networks': networks, 'messages':error_messages[2]})
        elif returncode == 3: # docker pull
            return render(
                request, 'create_docker.html',
                {'citas':send_frases,'networks': networks, 'messages':error_messages[1]})
        else:
            return render(
                request, 'create_docker.html',
                {'citas':send_frases,'networks': networks, 'messages':error_messages[6]})

    return render(
        request, 'create_docker.html',
        {'citas':send_frases,'networks': networks, 'messages':None})


def create_bbdd(request):
    # Set possible messages to show.
    error_messages = [
        ['BBDD creada con exito', 'good'], # 0
        ['Esta tabla ya existe', 'bad'], # 1
        ['Campos de formato incorrecto', 'bad'], # 2
        ['Numero de campos incorrecto (>1 <50)', 'bad'], # 3
        ['Tabla añadida con exito', 'good'], # 4
        ['Esta tabla no existe. No se puede borrar', 'bad'], # 5
        ['Tabla borrado con exito', 'good'], # 6
        ['El nombre de la BBDD es invalido o ya esta en uso', 'bad'], # 7
        ['Falta algun campo obligatorio', 'bad'], # 8
        ['Nombre de bbdd incorrecto', 'bad'], # 9
        ['PSQL no puede arrancar. ¿Lo tiene instalado en el sistema?', 'bad'], # 10
        ['Usuario / Password invalidos', 'bad'], # 11
        ['Ha habido algun error no identificado', 'bad'], # 12
        ['Algun campo es una palabra reservada de postgres', 'bad'], # 13
        ['El nombre de la tabla es incorrecto', 'bad'], # 14
        ['El nombre algun campo es incorrecto', 'bad'], # 15
        ['Ningun dato introducido', 'bad'], # 16
    ]

    send_frases = build_frases()

    # Get all tables
    print ('TOT: ', request.session.items())
    tables = request.session.get('tables', {})
    bbdd = request.session.get('bbdd', '')

    # Get all values of post method. Save in dict.
    fields = {}
    for key, values in request.POST.lists():
        fields[key]=values

    if request.POST.get('add_table'):
        # Get bbdd name. If exits, use this. If not use cookies. If not, error.
        bbdd_name = fields.get('bbdd_name')
        if bbdd_name:
            if len(bbdd_name[0].split()) == 1:
                bbdd = bbdd_name[0]
            else:
                return render(
                    request, 'create_bbdd.html',
                    {'citas':send_frases,'tables': tables, 'bbdd':bbdd, 'messages':error_messages[9]})
            first_char_bbdd_name = bbdd[0]
            if first_char_bbdd_name.isdigit():
                return render(
                    request, 'create_bbdd.html',
                    {'citas':send_frases,'tables': tables, 'bbdd':bbdd, 'messages':error_messages[9]})
        elif not bbdd:
            return render(
                request, 'create_bbdd.html',
                {'citas':send_frases,'tables': tables, 'bbdd':bbdd, 'messages':error_messages[8]})        

        # Get new table name
        table_name = fields.get('add_table_name')[0]
        if ' ' in table_name or table_name[0].isdigit() or table_name.strip() == '':
            return render(
                request, 'create_bbdd.html',
                {'citas':send_frases,'tables': tables, 'bbdd':bbdd, 'messages':error_messages[14]})

        # Get new table num fields
        table_campos = fields.get('add_table_campos')[0]
        
        table_campos = table_campos.split(' ')
        # If num fields is bigger than 50 or 0, error.
        if len(table_campos) > 50 or table_campos == [""]:
            return render(
                request, 'create_bbdd.html',
                {'citas':send_frases,'tables': tables, 'bbdd':bbdd, 'messages':error_messages[3]})
        else:
            for campo in table_campos:
                # Si alguno es una palabra reservada, error.
                if campo in RESERVED_WORDS_PSQL or campo.upper() in RESERVED_WORDS_PSQL:
                    return render(
                        request, 'create_bbdd.html',
                        {'citas':send_frases,'tables': tables, 'bbdd':bbdd, 'messages':error_messages[13]})
                if campo:
                    # Si alguno empieza por digito, error
                    if campo[0].isdigit():
                        return render(
                            request, 'create_bbdd.html',
                            {'citas':send_frases,'tables': tables, 'bbdd':bbdd, 'messages':error_messages[15]})

        # Check if this table name already exists. If yes, error. If no, create this on tables.
        if tables == {} or table_name not in tables.keys():
            tables[table_name] = {}
        else:
            return render(
                request, 'create_bbdd.html',
                {'citas':send_frases,'tables': tables, 'bbdd':bbdd, 'messages':error_messages[1]})

        # Create fields for this table name
        for campo in table_campos:
            if campo.strip() != '':
                tables[table_name][campo] = {'tipo': '', 'fk': [], 'pk': [], 'uk': '', 'nn':''}

        # Save values tables. Refresh our dict.
        refresh_tables_dict(tables, fields)

        # Update cookies.
        request.session['tables'] = tables
        print ('ENVII: ', request.session['tables'])
        request.session['bbdd'] = bbdd

        # Render.
        return render(
            request, 'create_bbdd.html',
            {'citas':send_frases,'tables': tables, 'bbdd':bbdd, 'messages':error_messages[4]})

    # delete for table name and exact number of fields
    if request.POST.get('delete_table'):
        # Get table name to delete
        table_name = fields.get('add_table_name')[0]

        # Check if this table name already exists. If no, error. If yes, delete this.
        if tables != {} and table_name in tables.keys():
            del tables[table_name]
        else:
            return render(
                request, 'create_bbdd.html',
                {'citas':send_frases,'tables': tables, 'bbdd':bbdd, 'messages':error_messages[5]})

        # Update cookies.
        request.session['tables'] = tables
        
        # Render.
        return render(
            request, 'create_bbdd.html',
            {'citas':send_frases,'tables': tables, 'bbdd':bbdd, 'messages':error_messages[6]})

    if request.POST.get('restart'):
        tables = {}
        bbdd = ''
        request.session['tables'] = {}
        request.session['bbdd'] = ''
        request.session.flush()
        return render(
            request, 'create_bbdd.html',
            {'citas':send_frases,'tables': tables, 'bbdd':bbdd, 'messages':None})        

    if request.POST.get('refresh'):
        refresh_tables_dict(tables, fields)
        request.session['tables'] = tables
        request.session['bbdd'] = bbdd
        return render(
            request, 'create_bbdd.html',
            {'citas':send_frases,'tables': tables, 'bbdd':bbdd, 'messages':None})

    if request.POST.get('Get_script'):

        # Save values tables. Refresh our dict.
        refresh_tables_dict(tables, fields)

        # Build script to create database
        if fields:
            final_script = build_bbdd_script(fields, bbdd)
        else:
            return render(
                request, 'create_bbdd.html',
                {'citas':send_frases,'tables': tables, 'bbdd':bbdd, 'messages':error_messages[16]})

        # Send file to client
        string_to_return = final_script
        file_to_send = ContentFile(string_to_return)
        response = HttpResponse(file_to_send,'plain/text')
        response['Content-Length'] = file_to_send.size    
        response['Content-Disposition'] = 'attachment; filename= {}'.format('script_gui.sql')
        return response

    if request.POST.get('Go'):

        # Start postgresql. If cant, psql maybe no exists.
        start_psql = popen_returncode(['systemctl', 'start', 'postgresql'])
        time.sleep(2)
        if start_psql != 0:
            return render(
                request, 'create_bbdd.html',
                {'citas':send_frases,'tables': tables, 'bbdd':bbdd, 'messages':error_messages[10]})

        # Save values tables. Refresh our dict.
        refresh_tables_dict(tables, fields)

        if fields and bbdd:
            final_script = build_bbdd_script(fields, bbdd)
        else:
            return render(
                request, 'create_bbdd.html',
                {'citas':send_frases,'tables': tables, 'bbdd':bbdd, 'messages':error_messages[16]})

        # Save file. For run script later.
        file = open('/tmp/script_gui.sql', 'w')
        file.write(final_script)
        file.close()

        user = fields['user'][0]
        password = fields['password'][0]
        if user == "" or password == "":
            return render(
                request, 'create_bbdd.html',
                {'citas':send_frases,'tables': tables, 'bbdd':bbdd, 'messages':error_messages[8]})        
            
        # Go and get return code
        returncode = popen_stdout(["/etc/guitool/script_programs/create_bbdd.sh", user, password])
        if returncode[:-1] == "esto_ha_ido_bien":
            return render(
                request, 'create_bbdd.html',
                {'citas':send_frases,'tables': tables, 'bbdd':bbdd, 'messages':error_messages[0]})
        else:
            return render(
                request, 'create_bbdd.html',
                {'citas':send_frases,'tables': tables, 'bbdd':bbdd, 'messages':[returncode, 'bad']})

    return render(
        request, 'create_bbdd.html',
        {'citas':send_frases,'tables': tables, 'bbdd':bbdd, 'messages':None})


def show_search(request):

    send_frases = build_frases()

    # Parse query
    parsed_url = urlparse(request.build_absolute_uri())
    fields = parse_qs(parsed_url.query)
    query = fields.get('query')

    # if no query return without tools
    if not query:
        return render(
            request, 'search_tool.html',
            {'citas':send_frases,'tools': []})    
    query = query[0]
    
    # search tools
    tools = Herramientas.objects.filter(descripcion__icontains=query).values()

    return render(
        request, 'search_tool.html',
        {'citas':send_frases,'tools': tools})
