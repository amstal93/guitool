#! /bin/bash
# Marc Serra
fuser -k 8000/tcp 2> /dev/null
screen -d -m -S DjangoServer bash -c 'python /etc/guitool/GUIT_LinuxAdministration/manage.py runserver 0.0.0.0:8000' 2> /dev/null
google-chrome http://127.0.0.1:8000/base --no-sandbox  &> /dev/null &


