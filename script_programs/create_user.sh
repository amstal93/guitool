#! /bin/bash
username=$1
password=$2
group_main=$3
groups_sec=$4
home_dir=$5
nombre=$6
direccion=$7
telefono=$8

mkdir -p $home_dir

if [ "$group_main" == 'default' -a "$groups_sec" == 'None' ]
	then
	useradd --home $home_dir --create-home --system $username
	if [ $? -ne 0 ]
	then
	  exit 1
	fi

elif [ "$group_main" == 'default' -a "$groups_sec" != 'None' ]
	then
	useradd --home $home_dir --create-home --groups $groups_sec --system $username 
	if [ $? -ne 0 ]
	then
	  exit 2
	fi

elif [ "$group_main" != 'default' -a "$groups_sec" == 'None' ]
	then
	useradd --home $home_dir --create-home --gid $group_main  --system $username 
	if [ $? -ne 0 ]
	then
	  exit 3
	fi

elif [ "$group_main" != 'default' -a "$groups_sec" != 'None' ]
	then
		useradd --home $home_dir --create-home --gid $group_main --groups $groups_sec  --system $username 
	if [ $? -ne 0 ]
	then
	  exit 4
	fi

fi

if [ "$nombre" != 'None' ]
	then
	chfn -f "$nombre" $username
	if [ $? -ne 0 ]
	then
	  exit 5
	fi
fi
if [ "$direccion" != 'None' ] 
	then
	chfn -o "$direccion" $username
	if [ $? -ne 0 ]
	then
	  exit 6
	fi
fi
if [ "$telefono" != 'None' ]
	then
	chfn -h "$telefono" $username
	if [ $? -ne 0 ]
	then
	  exit 7
	fi
fi
echo "$password" | passwd $username --stdin
if [ $? -ne 0 ]
then
  exit 8
fi
exit 0
