# Dietari - Marc Serra Hidalgo

Nota: Abans de la creació d’aquest dietari cal comentar que desde feia unes tres o quatre setmanes ja s’havia començat amb el desenvolupament del projecte. No es treballava cada dia, però si els caps de setmana, alguna nit d’entre setmana i durant les vacances quan tenia temps. Calculo que fins al moment hi he dedicat al voltant d’unes 50-60 hores (sense comptar les hores de lectura prèvies del llibre manual que disposo sobre DJANGO i d’altres documentacions online). Tot el que s’ha desenvolupat fins ara es detalla a la llista següent:
- Creacio del projecte de DJANGO, l'entorn de treball i l'entorn virtual.
- Creacio dels scripts de instal·lació de la aplicació.
- Creacio dels scripts de posada en marxa rapida de la aplicació.
- Creacio del disseny web i del model de plantilles
- Començada l’eina: crear usuaris (practicament acabada).
- Començada l’eina: crear rutines de backups (practicament acabada).

A partir d'aquest moment, el dietari es el següent:

## Dia 17/05/2018
### Objectius: 
- Revisar i acabar documentacio
- Revisar plantilla demo
- Acabar presentacio
- Polir detalls codi i tot
- Simular la presentacio a un altre PC

### Resultats:
#### Objectius Completats? Si no, perque?
Si
#### S'ha fet alguna altra cosa?
No

-------------------------------------------------

## Dia 16/05/2018
### Objectius: 
- Generar documentacio
- Redactar format demo
- Generar presentacio
- Polir detalls
- Realitzar proves per la demo

### Resultats:
#### Objectius Completats? Si no, perque?
Si
#### S'ha fet alguna altra cosa?
No

-------------------------------------------------

## Dia 15/05/2018
### Objectius: 
- Generar documentacio
- Pensar format presentacio
- Realitzar proves per la demo

### Resultats:
#### Objectius Completats? Si no, perque?
Si
#### S'ha fet alguna altra cosa?
No

--------------------------------------------------

## Dia 14/05/2018
### Objectius: 
- Generar documentacio
- Pensar format presentacio

### Resultats:
#### Objectius Completats? Si no, perque?
Si
#### S'ha fet alguna altra cosa?
No

--------------------------------------------------

## Dia 11/05/2018
### Objectius: 
- Generar documentacio

### Resultats:
#### Objectius Completats? Si no, perque?
Si
#### S'ha fet alguna altra cosa?
No

--------------------------------------------------

## Dia 10/05/2018
### Objectius: 
- Generar documentacio

### Resultats:
#### Objectius Completats? Si no, perque?
Si
#### S'ha fet alguna altra cosa?
No

--------------------------------------------------


## Dia 09/05/2018
### Objectius: 
- Seguir amb la seguent eina (BBDD) - comprovacions d'errors
- Fer bbdd django per buscador.

### Resultats:
#### Objectius Completats? Si no, perque?
Si
#### S'ha fet alguna altra cosa?
No

--------------------------------------------------

## Dia 08/05/2018
### Objectius: 
- Seguir amb la seguent eina (BBDD) - comprovacions d'errors
- Canviar firefox per chrome a navegador de us

### Resultats:
#### Objectius Completats? Si no, perque?
Si
#### S'ha fet alguna altra cosa?
No

--------------------------------------------------

## Dia 04/05/2018
### Objectius: 
- Seguir amb la seguent eina (BBDD) - comprovacions d'errors
- Generar documentacio

### Resultats:
#### Objectius Completats? Si no, perque?
No. No s'ha generat documentacio.
#### S'ha fet alguna altra cosa?
Si. Fer una pijada: les citas computacionales.

--------------------------------------------------


## Dia 03/05/2018
### Objectius: 
- Seguir amb la seguent eina (BBDD) - afegir not null de opcions, revisar codi, millorar html, fer creacio de script

### Resultats:
#### Objectius Completats? Si no, perque?
Si.
#### S'ha fet alguna altra cosa?
No.

--------------------------------------------------


## Dia 02/05/2018
### Objectius: 
- Seguir amb la seguent eina (BBDD) - aconseguir que es mantinguin les dades del formulari cada cop que es prem un button

### Resultats:
#### Objectius Completats? Si no, perque?
Si.
#### S'ha fet alguna altra cosa?
No.

--------------------------------------------------

## Dia 01/05/2018
### Objectius: 
- Seguir amb la seguent eina (BBDD)

### Resultats:
#### Objectius Completats? Si no, perque?
Si.
#### S'ha fet alguna altra cosa?
No.

--------------------------------------------------

## Dia 30/04/2018
### Objectius: 
- Seguir amb la seguent eina (BBDD) - crear pantilla html i començar codi

### Resultats:
#### Objectius Completats? Si no, perque?
Si.
#### S'ha fet alguna altra cosa?
No.

--------------------------------------------------

## Dia 29/04/2018
### Objectius: 
- Plantejar la seguent eina (BBDD)

### Resultats:
#### Objectius Completats? Si no, perque?
Si.
#### S'ha fet alguna altra cosa?
No.

--------------------------------------------------

## Dia 27/04/2018
### Objectius: 
- Seguir amb la seguent eina (docker)
- Generar documentacio
- Perfeccionar la plantilla base
- Revisar codi altres eines

### Resultats:
#### Objectius Completats? Si no, perque?
Si.
#### S'ha fet alguna altra cosa?
No.

--------------------------------------------------

## Dia 26/04/2018
### Objectius: 
- Generar documentació
- Revisar codi eines ja creades
- Seguir amb la seguent eina (docker)

### Resultats:
#### Objectius Completats? Si no, perque?
No. No s'ha generat documentacio ni s'ha revisat codi de les eines anteriors.

#### S'ha fet alguna altra cosa?
S'ha progressat molt amb la seguent eina.

--------------------------------------------------

## Dia 25/04/2018
### Objectius: 
- Generar documentació
- Revisar codi eines ja creades
- Començar a plantejar la seguent eina

### Resultats:
#### Objectius Completats? Si no, perque?
No. Codi d'altres eines no revisat.

#### S'ha fet alguna altra cosa?
No.

--------------------------------------------------

## Dia 24/04/2018
### Objectius: 
- Generar documentació

### Resultats:
#### Objectius Completats? Si no, perque?
Si
#### S'ha fet alguna altra cosa?
No

--------------------------------------------------

## Dia 20/04/2018
### Objectius: 
- Generar documentació
- Revisar scripts de instalacio 

### Resultats:
#### Objectius Completats? Si no, perque?
Si
#### S'ha fet alguna altra cosa?
No

--------------------------------------------------

## Dia 19/04/2018
### Objectius: 
- Generar documentació 

### Resultats:
#### Objectius Completats? Si no, perque?
Si
#### S'ha fet alguna altra cosa?
No

--------------------------------------------------

## Dia 18/04/2018
### Objectius: 
- Adaptar l’estructura del projecte als requeriments que abans no es sabien (arxius de documentacio, etc.)
- Generar documentació

### Resultats:
#### Objectius Completats? Si no, perque?
Si
#### S'ha fet alguna altra cosa?
No

--------------------------------------------------

## Dia 17/04/2018
### Objectius: 
- Incorporar tota la feina feta a la maquina de l’aula
- Aconseguir que els scripts de instalacio i posada en marxa de la eina creats previament funcionin a l’aula.
- Generar documentació

### Resultats:
#### Objectius Completats? Si no, perque?
No. No s'ha generat documentació i falta revisar alguna cosa dels scripts de instalacio.
#### S'ha fet alguna altra cosa?
No.

--------------------------------------------------

