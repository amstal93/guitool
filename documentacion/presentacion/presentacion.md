% GUI Tool for Linux System Administration
% Marc Serra Hidalgo
% isx41745190 - EdT - Curs 2017/2018

---

### 0. Estructura de la presentació

1. Tecnologies utilitzades

2. Que es GUI Tool for Linux System Administration?

3. Estructura del projecte

4. Eines desenvolupades

5. Demo de cada eina:
- Desmostrar que funciona.
- Desmostrar alguns dels errors controlats.

6. Explicació del backend de dues eines:
- Fluxe d'execució DJANGO.
- Codi python, html i bash.

---

### 1. Tecnologies utilitzades

- **Django**: Peça angular del projecte. Conecta la part lógica del sistema (scripts que modifiquen el sistema) amb la part lògica pròpia de DJANGO (*views* - funcions que processen les dades que s'envien dels formularis) i amb la part visual (códi HTML - CSS). A més, s'utiliza el servidor web que porta incorporat. Tot en un.

- **Python**: Llenguatge amb el que es desenvolupa tot el codi DJANGO. Principalment les views.

- **Bash**: Llenguatge amb el que es desenvolupen els *scripts* que modificarán el sistema, que faran algunes de les comprovacions de requerimients de les eines y que ens permetran fer l'instalacio i posada en marxa de la plataforma.

---

### 1. Tencologies utilitzades (2)

- **HTML / CSS**: Llenguatges amb els que es desenvolupa el diseny i l'estructura de la pagina web.

- **Protocol HTTP**: Protocol que es necesari conèixer para gestionar la comunicació entre el framework web i el navegador i així poder obtenir les dades que provenen dels formularis.

- **Altres tecnologies específiqiues**: Segons quina eina es la que es vol desenvolupar, es necessita tenir un coneixement profund sobre aquesta. Pel que fa a les eines que ja s'incorporen, es necessari coneixer: Dockers, Sql, aplicacions de bash, etc.

- **Altres tecnologies generals**:
	* Git per gestionar el reopsitori
	* Entorns virtuals per crear-lo i executar-lo
	* Altres

---

### 2. Que es GUI-Tool?

Eina gràfica que permet als administradors de sistemes Fedora realizar algunes de las accions de administració local de manera senzilla, ràpida y àgil gracies a una plataforma web adaptada per a aquest fi.

*Si no em voleu escoltar i desitjeu començar a MIRAR (**NO UTILITZAR**) l'eina, podeu obrir el navegador i entrar a [192.168.0.40:8000/base](http://192.168.0.40:8000/base) (habilitat solament per el dia de la presentació: variable **ALLOWED_HOSTS** del **setting.py**)*

![](https://betanews.com/wp-content/uploads/2016/04/penguingun-600x600.jpg)

--- 

### 3. Estructura del projecte DJANGO a alt nivell

Consulta [l'apartat](https://gitlab.com/isx41745190/guitool/blob/master/documentacion/guia_desarrolladores.md#3-estructura-del-repositorio) de la documentació per a desenvolupadors per a una explicació molt detallada d'aquest punt.

1. Carpeta del projecte
- **Script de comportament del servidor**: permet engegar el servidor, aturar-lo, aplicar canvis de bbdd, etc.
- **Carpeta plantilles html**: conté totes les plantilles *.html*.
- **Arxius estàtics**: conté les imatges, el css, etc.
- **Carpeta del codi**
	- **Arxiu de configuracions del projecte**(*settings.py*)
	- **Arxiu de les *views*** (*views.py*)
	- **Arxiu del urlmatching** (*urls.py*): crida a una *view* o una altre segons la url obtinguda.
	- **Arxius per a la configuracio del admin de DJANGO** (*models.py* i *admin.py*)
2. Scripts bash

---

#### 4. Eines desenvolupades

1. Crear usuaris
- Eina que permet crear un usuari del sistema amb password. Algunes característiques:
	- Es permet especificar un **directori HOME** o deixar el per defecte.
	- Es permet especificar quin sera el seu **grup principal** o deixar el per defecte.
	- Es permet especificar alguns aspectes del seu **GECOS** o deixar-ho en blanc. 
	- Es permet especificar els **grups secundaris** als quals pertany.
	- Altres..

---

#### 4. Eines desenvolupades (2)

2. Crear rutines de backup's
- Eina que permet crear una rutina de backup en el sistema. Algunes característiques:
	- Es permet especificar el **directori** en questió. 
	- Es permet especificar quin será el **timing** desitjat.
	- Es permet especificar si es desitja crear el **primer backup** ara mateix.
	- Tots els backups es guarden en un directori creat durant la instalació (Backups_guitool) i es troba en el HOME de root.

---

#### 4. Eines desenvolupades (3)

3. Crear contenidors Docker
- Eina que permet crear un contenidor de Docker. Algunes característiques:
	- Es permet especificar la **imatge base** a partir de la qual es genera el container (predefinida o pròpia).
	- Es permet especificar quin serà el seu **nom** i **hostname**.
	- Es permet especificar de quina **xarxa** formara part.
	- Es permeten especificar alguns **paquets de software** que es desitja que porti instal·lats per defecte.
	- Es permeten especificar si es desitja incloure **fitxers** així com si serà un Docker **privileged** o no.

---

#### 4. Eines desenvolupades (4)

4. Crear bases de dades psql
- Eina que permet crear una BBDD psql desde zero. Algunes característiques:
	- Es permet especificar les seves **taules**.
	- Es permet especificar el seus **camps**.
	- Es permeten modificar les **caracteristiques dels camps**.
	- Es permeten especificar les **relacions entre camps**.
	- Es permet obtenir l'**script** o **instal·lar-ho directament** en el psql.

---

#### 4. Eines desenvolupades (5)

5. A mes, un buscador de eines
- No es una eina pròpiament dita pero es una funcionalitat que s'ha implementat a la pagina web i que permet buscar mitjançant un cuadre de text l'eina que desitjem. Útil per quan la plataforma tingui moltes eines.


# 5. Demostracio de cada eina

![](https://linpack-for-tableau.com/uploads/actualites/livedemo-1.png)

---

## 6. Backend. Com funciona tot això per darrera?

Fluxe d'execució normal:

1. **Navegador:** l'usuari entra a algun apartat de la plataforma y es genera una *request*.
2. **Url *matching***: la url generada fa *match* amb una directiva del fitxer *urlsconf.py* i aquest crida a una o altre *view*.
3. **Processament**: s'executa el codi de la **view**.
4. **Renderitzacio**: a la resposta s'instancia una de les plantilles juntament amb les variables necesaries que modifiquen el seu contingut.
5. **Enviament de la resposta**.
6. **Navegador**: es mostra en el navegador

Aquest fluxe es repeteix quan l'usuari clica al "Send" d'una eina, amb la diferencia de que ara, degut a com estan codificades les *views*, s'entrará a una part específica del codi de la *view* que es el que conté el comportament pròpiament dit de la eina.

Navegador --> Request --> Urlsconf --> Views --> Render (plantilla + enviament) --> Navegador

# Gràcies per la vostra atenció!
## Alguna pregunta?

![](https://cdn-images-1.medium.com/max/2000/1*y32goTcrvncb8ot3jcTsEA.png)

---

Marc Serra Hidalgo

Administració de Sistemes Informàtics en Xarxa

Curs 2017/2018

isx41745190

Escola del Treball - Barcelona