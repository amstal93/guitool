#!/bin/bash
fuser -k 8000/tcp 2> /dev/null
# firefox http://127.0.0.1:8000/base  &> /dev/null &
google-chrome http://127.0.0.1:8000/base --no-sandbox  &> /dev/null &
terminator -e 'exec python manage.py runserver 0.0.0.0:8000' &> /dev/null &

